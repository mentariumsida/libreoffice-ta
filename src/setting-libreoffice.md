# Setting Libreoffice

Buka menu `Tools` > `Options`.

### LibreOffice

- User Data (lengkapi data)

### Language Settings

- Languages:
    - Format
    - Locale settings: Indonesian

### LibreOffice Writer

Pengaturan ini penting dilakukan, sebab satuan yang digunakan adalah `cm`.

- General
    - Settings
    - Measurement unit: Centimeter

### LibreOffice Base

- Database
    - (Masukkan odb jika ada)
