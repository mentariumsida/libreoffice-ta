# Summary

- [Home](./index.md)
- [Setting libreoffice](./setting-libreoffice.md)
- [Paragraph style](./paragraph-style.md)
- [Page style](./page-style.md)
- [Character style](./character-style.md)
- [Frame style](./frame-style.md)
- [List style](./list-style.md)
- [Caption style](./caption-style.md)
- [Insert variable](./insert-variable.md)
- [Chapter numbering](./chapter-numbering.md)
- [Insert caption](./insert-caption.md)
- [Table of content](./table-of-content.md)
- [Bibliography](./bibliography.md)
