> Ketika mengatur style ini buatlah dari paling belakang dahulu.
>
>   1. Halaman isi
>   2. Halaman bab
>   3. Halaman awal
>   4. Halaman sampul

## Halaman Sampul

- Organizer
    - Style
        - Name: `1 Halaman Sampul`
        - Next Style: 2 Halaman Awal
- Page
    - Paper Format
        - Format: A4
        - Orientation: Portrait
    - Margins
        - Left: 4,00 cm
        - Right: 3,00 cm
        - Top: 4,00 cm
        - Bottom: 3,00 cm
    - Layout Settings
        - Page numbers: None
- Header
    - Header
        - Header: off
- Footer
    - Footer
        - Footer: off

## Halaman Awal

- Organizer
    - Style
        - Name: `2 Halaman Awal`
        - Next Style: 2 Halaman Awal
- Page
    - Paper Format
        - Format: A4
        - Orientation: Portrait
    - Margins
        - Left: 4,00 cm
        - Right: 3,00 cm
        - Top: 1,50 cm
        - Bottom: 1,50 cm
    - Layout Settings
        - Page numbers: i, ii, iii, ...
- Header
    - Header
        - Header: on
        - Spacing: 2,00 cm
        - Height: 0,50 cm
- Footer
    - Footer
        - Footer: on
        - Spacing: 1,00 cm
        - Height: 0,50 cm

## Halaman Bab

- Organizer
    - Style
        - Name: `3 Halaman Bab`
        - Next Style: 4 Halaman Isi
- Page
    - Paper Format
        - Format: A4
        - Orientation: Portrait
    - Margins
        - Left: 4,00 cm
        - Right: 3,00 cm
        - Top: 1,50 cm
        - Bottom: 1,50 cm
    - Layout Settings
        - Page numbers: 1,2,3, ...
- Header
    - Header
        - Header: on
        - Spacing: 2,00 cm
        - Height: 0,50 cm
- Footer
    - Footer
        - Footer: on
        - Spacing: 1,00 cm
        - Height: 0,50 cm

## Halaman Isi

- Organizer
    - Style
        - Name: `4 Halaman Isi`
        - Next Style: 4 Halaman Isi
- Page
    - Paper Format
        - Format: A4
        - Orientation: Portrait
    - Margins
        - Left: 4,00 cm
        - Right: 3,00 cm
        - Top: 1,50 cm
        - Bottom: 1,50 cm
    - Layout Settings
        - Page numbers: 1,2,3, ...
- Header
    - Header
        - Header: on
        - Spacing: 2,00 cm
        - Height: 0,50 cm
- Footer
    - Footer
        - Footer: on
        - Spacing: 1,00 cm
        - Height: 0,50 cm
