# Chapter Numbering

Tools > Chapter Numbering

## Numbering

- Level: 1
    - Number: 1,2,3, ...
    - Before: `BAB ` (BAB dengan spasi)
- Level: 2
    - Number: 1,2,3, ...
    - Show sublevels: 2
- Level: 3
    - Number: 1,2,3, ...
    - Show sublevels: 3
- Level: 4
    - Number: 1,2,3, ...
    - Show sublevels: 4
- Level: 5
    - Number: 1,2,3, ...
    - Show sublevels: 5

## Position

- Level: 1
    - Aligned at: 0,00 cm
    - Numbering followed by: Nothing
    - Indent at: 0,00 cm
- Level: 2
    - Aligned at: 0,00 cm
    - Numbering followed by: Tab stop
    - Tab stop at: 1,02 cm
    - Indent at: 1,02 cm
- Level: 3
    - Aligned at: 0,00 cm
    - Numbering followed by: Tab stop
    - Tab stop at: 1,27 cm
    - Indent at: 1,27 cm
- Level: 4
    - Aligned at: 0,00 cm
    - Numbering followed by: Tab stop
    - Tab stop at: 1,52 cm
    - Indent at: 1,52 cm
