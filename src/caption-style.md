# Caption Style

## Gambar

Paragraf style > `Caption` > `Gambar`

- Organizer
    - Style
        - Next style: Text Body Indent
- Indent & Spacing
    - Indent
      - Before text: 0 cm
      - After text: 0 cm
      - First line: 0 cm
    - Spacing
      - Above paragraph: 0,50 cm
      - Below paragraph: 0,50 cm
    - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
  - Font Effects
    - Effects
      - Case: (Without)

## Tabel

Paragraf style > `Caption` > `Tabel`

- Organizer
    - Style
      Next style: Text Body Indent
- Indent & Spacing
    - Indent
      - Before text: 0 cm
      - After text: 0 cm
      - First line: 0 cm
    - Spacing
      - Above paragraph: 0,50 cm
      - Below paragraph: 0,50 cm
    - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)

## Persamaan

Paragraf style > `Caption` > `Persamaan`

- Organizer
    - Style
      - Next style: Text Body Indent
- Indent & Spacing
    - Indent
        - Before text: 1 cm
        - After text: 0 cm
        - First line: 0 cm
    - Spacing
      - Above paragraph: 0,50 cm
      - Below paragraph: 0,50 cm
    - Line Spacing: 1.5 Lines
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)
- Tabs
    - Position: 14,0 cm

## Listing

Paragraf style > `Caption` > `Listing`

- Organizer
    - Style
        - Next style: Text Body Indent
- Indent & Spacing
    - Indent
        - Before text: 0 cm
        - After text: 0 cm
        - First line: 0 cm
    - Spacing
      - Above paragraph: 0,50 cm
      - Below paragraph: 0,50 cm
    - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)
