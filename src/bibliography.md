# Bibliography

Umumnya digunakan untuk membuat daftar rujukan atau pustaka dengan menggunakan APA Style.

Menu `Insert` >`Table of contents and indexes` > `Table Of Contents, Index, or Bibliography`.

- Type
  - Type and title:
    - Title: (Kosongi)
    - Tipe: Bibliography
  - Formatting of the Entries
    - Bracket: `()`

Masuk ke tab `Entry`.

## Book (Buku)

`Au. (Ye). Ti. Bo, Ad: Pu.TUR`

Keterangan :
  - Au (Author) : Nama penulis buku
  - Ye (Year) : Tahun penerbitan buku
  - Ti (Title) : Judul bagian buku
  - Bo (Book Title) : Judul cover buku
  - Ad (Address) : Alamat penerbitan buku
  - Pu (Publisher) : Nama penerbit buku
  - T (Tab Stop) : Tab stop, nilainya 14,00 cm
  - UR (URL) : Alamat lengkap DOI/situs

## Conference proceeding (kumpulan paper)

`Au. (Ye). Ti. In (p. Pa). Ad. Pu.`

Keterangan :
  - Au (Author) : Nama penulis artikel
  - Ye (Year) : Tahun penerbitan prosiding
  - Ti (Title) : Judul artikel
  - In (Institution) : Nama konferensi atau seminar
  - Pa (Page) : Halaman artikel dalam prosiding
  - Ad (Address) : Alamat penyelenggara konferensi atau seminar
  - Pu (Publisher) : Nama penerbit prosiding

## Journal (Jurnal)

`Au. (Ye). Ti. Bo, Pu, Vo(Nu), Pa.TUR`

Keterangan :
  - Au (Author) : Nama penulis artikel
  - Ye (Year) : Tahun penerbitan jurnal
  - Ti (Title) : Judul bagian jurnal
  - Bo (Journal Title) : Judul jurnal
  - Pu (Publisher) : Nama jurnal
  - Vo (Volume) : Volume jurnal
  - Nu (Number) : Nomer jurnal
  - Pa (Page) : Halaman artikel dalam jurnal
  - T (Tab Stop) : Tab stop, nilainya 14,00 cm
  - UR (URL) : Alamat lengkap DOI/situs

## WWW Document (Situs)

`Au. (Ye). Ti. An. UR`

Keterangan :
  - Au (Author) : Nama penulis
  - Ye (Year) : Tahun, bulan, dan tanggal pembuatan dokumen
  - Ti (Title) : Judul dokumen
  - An (Annotation) : Tahun, bulan, dan tanggal pembuatan dokumen
  - UR (URL) : Alamat lengkap situs
