# Paragraph Style (Gaya paragraf)

## Heading 1

- Organizer
    - Style
    - Next style: Heading 2
- Indent & Spacing
    - Indent
        - Before text: 0 cm
        - After text: 0 cm
        - First line: 0 cm
    - Spacing
        - Above paragraph: 0 cm
        - Below paragraph: 1,50 cm
    - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 14 pt
- Font Effects
    - Effects
        - Case: UPPERCASE

## Heading 2

- Organizer
    - Style
        - Next style: Text Body
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,50 cm
            - Below paragraph: 0,50 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 14 pt
- Font Effects
    - Effects
        - Case: Capitalize Every Word

## Heading 3

- Organizer
    - Style
        - Next style: Text Body
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,40 cm
            - Below paragraph: 0,40 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 13 pt
- Font Effects
    - Effects
        - Case: Capitalize Every Word

## Heading 4

- Organizer
    - Style
        - Next style: Text Body
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,40 cm
            - Below paragraph: 0,40 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: Capitalize Every Word

## Heading 5

- Organizer
    - Style
        - Next style: Text Body
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,40 cm
            - Below paragraph: 0,40 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: Capitalize Every Word

## Title

- Organizer
    - Style
        - Next style: Text Body
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 2,00 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 14 pt
- Font Effects
    - Effects
        - Case: UPPERCASE

## Text Body

- Organizer
    - Style
        - Next style: Text Body Indent
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 0,00 cm
        - Line Spacing: Double
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)

## Text Body Indent

- Organizer
    - Style
        - Next style: Text Body Indent
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 1,20 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 0,00 cm
        - Line Spacing: Double
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)

## Bibliography Heading (Judul daftar pustaka)

- Organizer
    - Style
        - Next style: Bibliography Heading
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 1,50 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 14 pt
- Font Effects
    - Effects
        - Case: UPPERCASE

## Bibliography 1

- Organizer
    - Style
        - Next style: Bibliography 1
    - Indent & Spacing
        - Indent
            - Before text: 1 cm
            - After text: 0 cm
            - First line: 1 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 0,21 cm
        - Line Spacing: Double
- Alighment
    - Options: Justified
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt

- Font Effects
    - Effects
        - Case: UPPERCASE
- Tabs
    - Position: 14,0 cm

## Contents Heading (Judul daftar isi)

- Organizer
    - Style
        - Next style: Contents Heading
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 1,50 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 14 pt
- Font Effects
    - Effects
        - Case: UPPERCASE

## Contents 1 (Isi dari daftar isi)

- Organizer
    - Style
        - Next style: Contents 1
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 0,00 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Left
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)
- Tabs
    - Position: 14,0 cm

## Figure Index Heading (Judul daftar gambar)

- Organizer
    - Style
        - Next style: Figure Index Heading
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 1,50 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 14 pt
- Font Effects
    - Effects
        - Case: UPPERCASE

## Figure Index 1

- Organizer
    - Style
        - Next style: Figure Index 1
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 0 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Left
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)
- Tabs
    - Position: 14,0 cm

## Table Index Heading

- Organizer
    - Style
        - Next style: Table Index Heading
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 1,50 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Center
- Font
    - Family: Liberation Serif
    - Style: Bold
    - Size: 14 pt
- Font Effects
    - Effects
        - Case: UPPERCASE

## Table Index 1

- Organizer
    - Style
        - Next style: Table Index 1
    - Indent & Spacing
        - Indent
            - Before text: 0 cm
            - After text: 0 cm
            - First line: 0 cm
        - Spacing
            - Above paragraph: 0,00 cm
            - Below paragraph: 0,00 cm
        - Line Spacing: 1.5 Lines
- Alighment
    - Options: Left
- Font
    - Family: Liberation Serif
    - Style: Regular
    - Size: 12 pt
- Font Effects
    - Effects
        - Case: (Without)
- Tabs
    - Position: 14,0 cm
