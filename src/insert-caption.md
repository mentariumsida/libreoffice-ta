# Insert Caption

## Gambar
  Menu `Insert` > `Caption` > `Gambar`
- Caption: Nama Gambar
- Properties
    - Category: Gambar
    - Numbering: Arabic(1 2 3)
    - Position: Bellow

## Tabel
  Menu `Insert` > `Caption` > `Tabel`
- Caption: Nama Tabel
- Properties
    - Category: Tabel
    - Numbering: Arabic(1 2 3)
    - Position: Above (khusus tabel ada di atas)

## Persamaan
  Menu `Insert` > `Caption` > `Persamaan`
- Caption: Nama Persamaan
- Properties
    - Category: Persamaan
    - Numbering: Arabic(1 2 3)
    - Position: Bellow

## Listing
  Menu `Insert` > `Caption` > `Listing`
- Caption: Nama Listing
- Properties
    - Category: Listing
    - Numbering: Arabic(1 2 3)
    - Position: Bellow
