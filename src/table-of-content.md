# Table of content

Digunakan untuk membuat daftar isi, daftar gambar dan daftar tabel.

Menu `Insert` > `Table of contents and indexes` > `table of contents, index, or bibliography`.

- Type
  - Type and title:
    - Title: (Kosongi)
    - Tipe: Table of Contents
  Create Index or Table of Contents
    - Evaluate up to level: 5

Masuk ke tab `Entries`

Keterangan :

- `LS` (Hyperlink Start): Awal hiperlink
- `E#` (Chapter number) : Nomer BAB
- `E` (Entry Character Style) : Judul
- `T` (Tab Stop) : Tab stop
- `#` (Page Number) : Nomer halaman
- `LE` (Hyperlink End): Akhir dari hiperlink

Setiap level memiliki struktur yang sama, yaitu:

`LS`-`E#`-`E `-`T`-`T`-`LE` (setelah `E` ditambahkan spasi)

Di bagian kotak teks antara `E` dan `T` awal, berikan spasi 1 kali.

Bagian `T` (tabstop) kedua, ceklis `align right`.

- Fill character: (kosong)

`T` (tabstop) awal memiliki posisi tabstop tersendiri:

- Fill character: `.`

Nilai level tabstop awal:
- Level 1: 13,00 cm
- Level 2: 12,50 cm
- Level 3: 12,00 cm
- Level 4: 11,50 cm
- Level 5: 11,00 cm
- Level 6: 10,50 cm
- Level 7: 10,00 cm
- Level 8: 9,50 cm
- Level 9: 9,00 cm
- Level 10: 8,50 cm

Khusus `E` level 1, Character style: Main Index Entry
