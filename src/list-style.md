# List Styles

## Penomeran

New > `Penomeran`

- Organizer
    - Style
        - Name: Penomeran
- Numbering Style
    - Selection
        - Number 1)2)3)
- Customize
    - Level: 1
        - Number: 1,2,3, ...
        - Character Style: Numbering Symbols
        - After: )
    - Level: 2
        - Number: a,b,c, ...
        - Character Style: Numbering Symbols
        - Show Sublevels: 1
        - Before: (
        - After: )
    - Level: 3
        - Number: Bullet
        - Character Style: Bullets
    - Level: 4
        - Number: i,ii,iii, ...
        - Character Style: Numbering Symbols
        - Show Sublevels: 1
        - After: )
    - Level: 5
        - Number: Bullet
        - Character Style: Bullets
- Position
    - Level: 1
        - Aligned at: 0,50 cm
        - Numbering followed by: Tab stop
        - Tab stop at: 0,00 cm
        - Indent at: 1,20 cm
    - Level: 2
        - Aligned at: 1,00 cm
        - Numbering followed by: Tab stop
        - Tab stop at: 0,00 cm
        - Indent at: 1,60 cm
    - Level: 3
        - Aligned at: 1,60 cm
        - Numbering followed by: Tab stop
        - Tab stop at: 0,00 cm
        - Indent at: 2,00 cm
    - Level: 4
        - Aligned at: 2,00 cm
        - Numbering followed by: Tab stop
        - Tab stop at: 0,00 cm
        - Indent at: 2,40 cm
    - Level: 5
        - Aligned at: 2,60 cm
        - Numbering followed by: Tab stop
        - Tab stop at: 0,00 cm
        - Indent at: 3,00 cm
