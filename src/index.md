# Deskripsi

Penerapan pembuatan template untuk seminar proposal dan tugas akhir. Template final dapat diunduh [di tautan ini](https://gitlab.com/hervyqa/umsida-template/-/archive/main/umsida-template-main.zip).

![LibreOffice](media/libreoffice-langitketujuh-id.webp)
