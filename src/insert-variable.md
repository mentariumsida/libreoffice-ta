# Insert Variable

## Tabel

  Menu `Insert` > `Field` > `More Field`

  `Variable` > `Number Range`
- Name: `Tabel`
- Format: Arabic (1 2 3)
- Numbering by chapter
    - Level: 1
    - Separator: `.`

## Gambar
  Menu `Insert` > `Field` > `More Field`

  `Variable` > `Number Range`
- Name: `Gambar`
- Format: Arabic (1 2 3)
- Numbering by chapter
    - Level: 1
    - Separator: `.`

## Persamaan
  Menu `Insert` > `Field` > `More Field`

  `Variable` > `Number Range`
- Name: `Persamaan`
- Format: Arabic (1 2 3)
- Numbering by chapter
    - Level: 1
    - Separator: `.`

## Listing
  Menu `Insert` > `Field` > `More Field`

  `Variable` > `Number Range`
- Name: `Listing`
- Format: Arabic (1 2 3)
- Numbering by chapter
    - Level: 1
    - Separator: `.`
